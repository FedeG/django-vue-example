// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import LinksDetail from '../components/LinksDetail'

Vue.config.productionTip = false

window.render_components = properties => {
  window.params = {...properties}
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    data: {
      links: properties.links
    },
    template: '<LinksDetail :links="links"/>',
    components: { LinksDetail }
  })
}

if (module.hot) {
  if (window.params) window.render_components(window.params)
  module.hot.accept()
}
